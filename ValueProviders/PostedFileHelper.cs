﻿using System;
using System.Web;

namespace Forloop.ValueProviders
{

    #region License

    // Copyright (c) Microsoft Open Technologies, Inc.  All rights reserved.
    // Microsoft Open Technologies would like to thank its contributors, a list
    // of whom are at http://aspnetwebstack.codeplex.com/wikipage?title=Contributors.
    // 
    // Licensed under the Apache License, Version 2.0 (the "License"); you
    // may not use this file except in compliance with the License. You may
    // obtain a copy of the License at
    // 
    // http://www.apache.org/licenses/LICENSE-2.0
    // 
    // Unless required by applicable law or agreed to in writing, software
    // distributed under the License is distributed on an "AS IS" BASIS,
    // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
    // implied. See the License for the specific language governing permissions
    // and limitations under the License.
    //
    // from the MVC framerwork HttpPostedFileBaseModelBinder class.

    #endregion

    public static class PostedFileHelper
    {
        public static HttpPostedFileBase ChooseFileOrNull(HttpPostedFileBase rawFile)
        {
            // case 1: there was no <input type="file" ... /> element in the post
            if (rawFile == null)
            {
                return null;
            }

            // case 2: there was an <input type="file" ... /> element in the post, but it was left blank
            if (rawFile.ContentLength == 0 && String.IsNullOrEmpty(rawFile.FileName))
            {
                return null;
            }

            // case 3: the file was posted
            return rawFile;
        }
    }
}