﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CsvHelper;
using CsvHelper.Configuration;

namespace Forloop.ValueProviders
{
    public class CsvValueProvider : IValueProvider
    {
        public static readonly CsvConfiguration Configuration = new CsvConfiguration { AllowComments = true };

        private readonly Lazy<PrefixContainer> _prefixContainer;
        private readonly NameValueCollection _values = new NameValueCollection();

        public CsvValueProvider(ControllerContext controllerContext)
        {
            _prefixContainer = new Lazy<PrefixContainer>(() => new PrefixContainer(_values.AllKeys), true);

            var files = controllerContext.HttpContext.Request.Files;

            if (files.Count == 0)
            {
                return;
            }

            var mapping = new List<KeyValuePair<string, HttpPostedFileBase>>();
            for (var i = 0; i < files.Count; i++)
            {
                var key = files.AllKeys[i];
                if (key != null)
                {
                    HttpPostedFileBase file = PostedFileHelper.ChooseFileOrNull(files[i]);

                    // we only care about csv files
                    if (file != null && !file.FileName.EndsWith(".csv"))
                    {
                        continue;
                    }

                    mapping.Add(new KeyValuePair<string, HttpPostedFileBase>(key, file));
                }
            }

            var fileGroups = mapping.GroupBy(el => el.Key, el => el.Value, StringComparer.OrdinalIgnoreCase);

            foreach (var fileGroup in fileGroups)
            {
                foreach (var file in fileGroup)
                {
                    if (file == null)
                    {
                        _values.Add(fileGroup.Key, string.Empty);
                    }
                    else
                    {
                        using (var reader = new CsvReader(new StreamReader(file.InputStream), Configuration))
                        {
                            int index = 0;
                            long previousCharPosition = 0;
                            while (reader.Read())
                            {
                                long charPosition = reader.Parser.CharPosition;

                                for (var j = 0; j < reader.CurrentRecord.Length; j++)
                                {
                                    var subPropertyName = reader.FieldHeaders[j].Trim();
                                    var value = reader.CurrentRecord[j];
                                    _values.Add(string.Format("{0}[{1}].{2}", fileGroup.Key, index, subPropertyName),
                                                value);
                                }

                                // naive way of determining if the file is *really* a csv file.
                                // the csv parser's character position does not change when it can't read
                                // the file as a csv.
                                if (charPosition == previousCharPosition)
                                {
                                    return;
                                }

                                previousCharPosition = charPosition;
                                index++;
                            }
                        }
                    }
                }
            }
        }

        public bool ContainsPrefix(string prefix)
        {
            return _prefixContainer.Value.ContainsPrefix(prefix);
        }

        public ValueProviderResult GetValue(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            if (!_values.AllKeys.Contains(key, StringComparer.OrdinalIgnoreCase))
            {
                return null;
            }

            var rawValue = _values.GetValues(key);
            var attemptedValue = _values[key];
            return new ValueProviderResult(rawValue, attemptedValue, CultureInfo.InvariantCulture);
        }
    }
}