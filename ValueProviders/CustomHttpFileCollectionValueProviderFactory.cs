﻿using System;
using System.Web.Mvc;

namespace Forloop.ValueProviders
{
    public class CustomHttpFileCollectionValueProviderFactory : ValueProviderFactory
    {
        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            if (controllerContext == null)
            {
                throw new ArgumentNullException("controllerContext");
            }

            return new CustomHttpFileCollectionValueProvider(controllerContext);
        }
    }
}