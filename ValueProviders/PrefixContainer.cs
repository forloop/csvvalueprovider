﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Forloop.ValueProviders
{

    #region License

    // Copyright (c) Microsoft Open Technologies, Inc.  All rights reserved.
    // Microsoft Open Technologies would like to thank its contributors, a list
    // of whom are at http://aspnetwebstack.codeplex.com/wikipage?title=Contributors.
    // 
    // Licensed under the Apache License, Version 2.0 (the "License"); you
    // may not use this file except in compliance with the License. You may
    // obtain a copy of the License at
    // 
    // http://www.apache.org/licenses/LICENSE-2.0
    // 
    // Unless required by applicable law or agreed to in writing, software
    // distributed under the License is distributed on an "AS IS" BASIS,
    // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
    // implied. See the License for the specific language governing permissions
    // and limitations under the License.
    //
    // this is an internal class in the framework which provides useful functionality for
    // custom ValueProviders

    #endregion

    internal class PrefixContainer
    {
        private readonly string[] _sortedValues;

        internal PrefixContainer(IEnumerable<string> values)
        {
            if (values == null)
            {
                throw new ArgumentNullException("values");
            }

            _sortedValues = values.Where(val => val != null).ToArray();
            Array.Sort(_sortedValues, StringComparer.OrdinalIgnoreCase);
        }

        internal static bool IsPrefixMatch(string prefix, string testString)
        {
            if (testString == null)
            {
                return false;
            }

            if (prefix.Length == 0)
            {
                return true; // shortcut - non-null testString matches empty prefix
            }

            if (prefix.Length > testString.Length)
            {
                return false; // not long enough
            }

            if (!testString.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
            {
                return false; // prefix doesn't match
            }

            if (testString.Length == prefix.Length)
            {
                return true; // exact match
            }

            // invariant: testString.Length > prefix.Length
            switch (testString[prefix.Length])
            {
                case '.':
                case '[':
                    return true; // known delimiters

                default:
                    return false; // not known delimiter
            }
        }

        internal bool ContainsPrefix(string prefix)
        {
            if (prefix == null)
            {
                throw new ArgumentNullException("prefix");
            }

            if (prefix.Length == 0)
            {
                return _sortedValues.Length > 0; // only match empty string when we have some value
            }

            return Array.BinarySearch(_sortedValues, prefix, new PrefixComparer(prefix)) > -1;
        }

        private class PrefixComparer : IComparer<string>
        {
            private readonly string _prefix;

            public PrefixComparer(string prefix)
            {
                _prefix = prefix;
            }

            public int Compare(string x, string y)
            {
                string testString = ReferenceEquals(x, _prefix) ? y : x;
                if (IsPrefixMatch(_prefix, testString))
                {
                    return 0;
                }

                return StringComparer.OrdinalIgnoreCase.Compare(x, y);
            }
        }
    }
}