﻿using System.Linq;
using System.Web.Mvc;

namespace Forloop.ValueProviders
{
    public class CsvValueProviderFactory : ValueProviderFactory
    {
        public static void AddToValueProviderFactoryCollection()
        {
            AddToValueProviderFactoryCollection(ValueProviderFactories.Factories);
        }

        public static void AddToValueProviderFactoryCollection(ValueProviderFactoryCollection collection)
        {
            var postedFileValueProviderFactory =
                collection.SingleOrDefault(x => x is System.Web.Mvc.HttpFileCollectionValueProviderFactory);

            if (postedFileValueProviderFactory != null)
            {
                var index = collection.IndexOf(postedFileValueProviderFactory);
                collection.Insert(index, new CustomHttpFileCollectionValueProviderFactory());
                collection.Remove(postedFileValueProviderFactory);
            }

            collection.Add(new CsvValueProviderFactory());
        }

        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            return new CsvValueProvider(controllerContext);
        }
    }
}