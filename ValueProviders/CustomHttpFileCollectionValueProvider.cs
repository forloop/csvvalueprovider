﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Forloop.ValueProviders
{

    #region License

    // Copyright (c) Microsoft Open Technologies, Inc.  All rights reserved.
    // Microsoft Open Technologies would like to thank its contributors, a list
    // of whom are at http://aspnetwebstack.codeplex.com/wikipage?title=Contributors.
    // 
    // Licensed under the Apache License, Version 2.0 (the "License"); you
    // may not use this file except in compliance with the License. You may
    // obtain a copy of the License at
    // 
    // http://www.apache.org/licenses/LICENSE-2.0
    // 
    // Unless required by applicable law or agreed to in writing, software
    // distributed under the License is distributed on an "AS IS" BASIS,
    // WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
    // implied. See the License for the specific language governing permissions
    // and limitations under the License.
    //
    // modified to ignore posted files whose file extension is in the list of those to ignore
    // Unfortunately this is a sealed class in the framework.

    #endregion

    public class CustomHttpFileCollectionValueProvider : DictionaryValueProvider<HttpPostedFileBase[]>
    {
        public static string[] IgnoreFileExtensions = new[] { "csv" };

        private static readonly Dictionary<string, HttpPostedFileBase[]> EmptyDictionary =
            new Dictionary<string, HttpPostedFileBase[]>();

        public CustomHttpFileCollectionValueProvider(ControllerContext controllerContext)
            : base(GetHttpPostedFileDictionary(controllerContext), CultureInfo.InvariantCulture)
        {
        }

        private static Dictionary<string, HttpPostedFileBase[]> GetHttpPostedFileDictionary(
            ControllerContext controllerContext)
        {
            HttpFileCollectionBase files = controllerContext.HttpContext.Request.Files;

            // fast-track common case of no files
            if (files.Count == 0)
            {
                return EmptyDictionary;
            }

            // build up the 1:many file mapping
            var mapping = new List<KeyValuePair<string, HttpPostedFileBase>>();
            var allKeys = files.AllKeys;
            for (int i = 0; i < files.Count; i++)
            {
                var key = allKeys[i];
                if (key != null)
                {
                    HttpPostedFileBase file = PostedFileHelper.ChooseFileOrNull(files[i]);

                    // ignore files with certain extensions. Adding these to the value provider
                    // will cause a HttpPostedFileBase instance to be returned for
                    // the model collection we want the CsvValueProvider to provide values for
                    if (file != null && IgnoreFileExtensions.Any(ext => file.FileName.EndsWith("." + ext)))
                    {
                        continue;
                    }

                    mapping.Add(new KeyValuePair<string, HttpPostedFileBase>(key, file));
                }
            }

            // turn the mapping into a 1:many dictionary
            var grouped = mapping.GroupBy(el => el.Key, el => el.Value, StringComparer.OrdinalIgnoreCase);
            return grouped.ToDictionary(g => g.Key, g => g.ToArray(), StringComparer.OrdinalIgnoreCase);
        }
    }
}