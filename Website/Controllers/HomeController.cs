﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Website.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View(new UsersModel());
        }

        [HttpPost]
        public ActionResult Index(UsersModel model)
        {
            return View(model);
        }
    }
}
