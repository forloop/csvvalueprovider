﻿using System.Collections.Generic;

namespace Website.Controllers
{
    public class UsersModel
    {
        public UsersModel()
        {
            Users = new List<User>();
        }

        public IList<User> Users { get; set; }
    }
}