﻿

namespace Website.Controllers
{
    using System.ComponentModel.DataAnnotations;

    public class User
    {
        public User()
        {
            Roles = new string[0];
        }

        [Required]
        public string FullName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public int? Age { get; set; }

        public string[] Roles { get; set; }
    }
}